From: =?utf-8?q?Jonas_=C3=85dahl?= <jadahl@gmail.com>
Date: Sat, 14 Nov 2020 09:41:23 +0000
Subject: renderer/native: Add udev rule to select primary GPU

Sometimes the automatically selected primary GPU isn't suitable with no
way to make an well educated guess to do it better. To make it possible
for the user to override the automatically calculated default, make it
possible to override it using a udev rule.

E.g. to select /dev/dri/card1 as the primary GPU, add a file e.g.
/usr/lib/udev/rules.d/61-mutter-primary-gpu.rules (path my vary
depending on distribution) containing the fellowing line:

ENV{DEVNAME}=="/dev/dri/card1", TAG+="mutter-device-preferred-primary"

Reboot or manual triggering of udev rules to make it take effect may be
required.

Related: https://gitlab.gnome.org/GNOME/mutter/merge_requests/1057

https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1562

Part-of: <https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1562>

(cherry picked from commit d622960429b2a10bc19d43a8fb56a73f99ecef10)

Origin: upstream, 3.38.2, commit:17e9cbe8cbeeedf4b50fe7a8af4d7adbde03dda0
---
 src/backends/native/meta-backend-native.c  |  3 +++
 src/backends/native/meta-kms-types.h       |  1 +
 src/backends/native/meta-renderer-native.c | 34 +++++++++++++++++++++++++++---
 src/backends/native/meta-udev.c            | 12 +++++++++++
 src/backends/native/meta-udev.h            |  2 ++
 5 files changed, 49 insertions(+), 3 deletions(-)

diff --git a/src/backends/native/meta-backend-native.c b/src/backends/native/meta-backend-native.c
index 38ff601..ed795cc 100644
--- a/src/backends/native/meta-backend-native.c
+++ b/src/backends/native/meta-backend-native.c
@@ -574,6 +574,9 @@ create_gpu_from_udev_device (MetaBackendNative  *native,
   if (meta_is_udev_device_requires_modifiers (device))
     flags |= META_KMS_DEVICE_FLAG_REQUIRES_MODIFIERS;
 
+  if (meta_is_udev_device_preferred_primary (device))
+    flags |= META_KMS_DEVICE_FLAG_PREFERRED_PRIMARY;
+
   device_path = g_udev_device_get_device_file (device);
 
   kms_device = meta_kms_create_device (native->kms, device_path, flags,
diff --git a/src/backends/native/meta-kms-types.h b/src/backends/native/meta-kms-types.h
index 1fed7e3..1d59025 100644
--- a/src/backends/native/meta-kms-types.h
+++ b/src/backends/native/meta-kms-types.h
@@ -57,6 +57,7 @@ typedef enum _MetaKmsDeviceFlag
   META_KMS_DEVICE_FLAG_BOOT_VGA = 1 << 0,
   META_KMS_DEVICE_FLAG_PLATFORM_DEVICE = 1 << 1,
   META_KMS_DEVICE_FLAG_REQUIRES_MODIFIERS = 1 << 2,
+  META_KMS_DEVICE_FLAG_PREFERRED_PRIMARY = 1 << 3,
 } MetaKmsDeviceFlag;
 
 typedef enum _MetaKmsPlaneType MetaKmsPlaneType;
diff --git a/src/backends/native/meta-renderer-native.c b/src/backends/native/meta-renderer-native.c
index eb11be9..87ca84b 100644
--- a/src/backends/native/meta-renderer-native.c
+++ b/src/backends/native/meta-renderer-native.c
@@ -64,6 +64,7 @@
 #include "backends/native/meta-drm-buffer-import.h"
 #include "backends/native/meta-drm-buffer.h"
 #include "backends/native/meta-gpu-kms.h"
+#include "backends/native/meta-kms-device.h"
 #include "backends/native/meta-kms-update.h"
 #include "backends/native/meta-kms-utils.h"
 #include "backends/native/meta-kms.h"
@@ -3725,6 +3726,21 @@ choose_primary_gpu_unchecked (MetaBackend        *backend,
    */
   for (allow_sw = 0; allow_sw < 2; allow_sw++)
   {
+    /* First check if one was explicitly configured. */
+    for (l = gpus; l; l = l->next)
+      {
+        MetaGpuKms *gpu_kms = META_GPU_KMS (l->data);
+        MetaKmsDevice *kms_device = meta_gpu_kms_get_kms_device (gpu_kms);
+
+        if (meta_kms_device_get_flags (kms_device) &
+            META_KMS_DEVICE_FLAG_PREFERRED_PRIMARY)
+          {
+            g_message ("GPU %s selected primary given udev rule",
+                       meta_gpu_kms_get_file_path (gpu_kms));
+            return gpu_kms;
+          }
+      }
+
     /* Prefer a platform device */
     for (l = gpus; l; l = l->next)
       {
@@ -3733,7 +3749,11 @@ choose_primary_gpu_unchecked (MetaBackend        *backend,
         if (meta_gpu_kms_is_platform_device (gpu_kms) &&
             (allow_sw == 1 ||
              gpu_kms_is_hardware_rendering (renderer_native, gpu_kms)))
-          return gpu_kms;
+          {
+            g_message ("Integrated GPU %s selected as primary",
+                       meta_gpu_kms_get_file_path (gpu_kms));
+            return gpu_kms;
+          }
       }
 
     /* Otherwise a device we booted with */
@@ -3744,7 +3764,11 @@ choose_primary_gpu_unchecked (MetaBackend        *backend,
         if (meta_gpu_kms_is_boot_vga (gpu_kms) &&
             (allow_sw == 1 ||
              gpu_kms_is_hardware_rendering (renderer_native, gpu_kms)))
-          return gpu_kms;
+          {
+            g_message ("Boot VGA GPU %s selected as primary",
+                       meta_gpu_kms_get_file_path (gpu_kms));
+            return gpu_kms;
+          }
       }
 
     /* Fall back to any device */
@@ -3754,7 +3778,11 @@ choose_primary_gpu_unchecked (MetaBackend        *backend,
 
         if (allow_sw == 1 ||
             gpu_kms_is_hardware_rendering (renderer_native, gpu_kms))
-          return gpu_kms;
+          {
+            g_message ("GPU %s selected as primary",
+                       meta_gpu_kms_get_file_path (gpu_kms));
+            return gpu_kms;
+          }
       }
   }
 
diff --git a/src/backends/native/meta-udev.c b/src/backends/native/meta-udev.c
index 41c686c..9568698 100644
--- a/src/backends/native/meta-udev.c
+++ b/src/backends/native/meta-udev.c
@@ -95,6 +95,18 @@ meta_is_udev_device_requires_modifiers (GUdevDevice *device)
   return g_strv_contains (tags, "mutter-device-requires-kms-modifiers");
 }
 
+gboolean
+meta_is_udev_device_preferred_primary (GUdevDevice *device)
+{
+  const char * const * tags;
+
+  tags = g_udev_device_get_tags (device);
+  if (!tags)
+    return FALSE;
+
+  return g_strv_contains (tags, "mutter-device-preferred-primary");
+}
+
 gboolean
 meta_udev_is_drm_device (MetaUdev    *udev,
                          GUdevDevice *device)
diff --git a/src/backends/native/meta-udev.h b/src/backends/native/meta-udev.h
index eb90abb..453cce9 100644
--- a/src/backends/native/meta-udev.h
+++ b/src/backends/native/meta-udev.h
@@ -34,6 +34,8 @@ gboolean meta_is_udev_device_boot_vga (GUdevDevice *device);
 
 gboolean meta_is_udev_device_requires_modifiers (GUdevDevice *device);
 
+gboolean meta_is_udev_device_preferred_primary (GUdevDevice *device);
+
 gboolean meta_udev_is_drm_device (MetaUdev    *udev,
                                   GUdevDevice *device);
 
