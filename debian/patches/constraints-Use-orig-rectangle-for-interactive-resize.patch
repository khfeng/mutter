From: Olivier Fourdan <ofourdan@redhat.com>
Date: Wed, 14 Oct 2020 10:30:53 +0200
Subject: constraints: Use "orig" rectangle for interactive resize

Bug 448183 fixed an issue with _NET_WM_MOVERESIZE_WINDOW not moving a
window by basing the resize on the current (new) rectangle instead of
the original rectangle.

While this fixes the issue with _NET_WM_MOVERESIZE_WINDOW, this also
causes windows with a size increment to move when the resize also
implies a move, such windows might drift while resizing.

Make sure to use the current rectangle for non-interactive resizes only.

(cherry picked from commit 7ab3eac0e27e62642bdd8dc1ddad3bf9acf5a02d)

Bug: https://gitlab.gnome.org/GNOME/mutter/-/issues/543
Bug-Ubuntu: https://bugs.launchpad.net/ubuntu/+source/mutter/+bug/1878293
Origin: upstream, 3.38.2
---
 src/core/constraints.c | 34 +++++++++++++---------------------
 1 file changed, 13 insertions(+), 21 deletions(-)

diff --git a/src/core/constraints.c b/src/core/constraints.c
index 168a55b..90e9a03 100644
--- a/src/core/constraints.c
+++ b/src/core/constraints.c
@@ -469,6 +469,16 @@ setup_constraint_info (ConstraintInfo      *info,
                 info->entire_monitor.width, info->entire_monitor.height);
 }
 
+static MetaRectangle *
+get_start_rect_for_resize (MetaWindow     *window,
+                           ConstraintInfo *info)
+{
+  if (!info->is_user_action && info->action_type == ACTION_MOVE_AND_RESIZE)
+    return &info->current;
+  else
+    return &info->orig;
+}
+
 static void
 place_window_if_needed(MetaWindow     *window,
                        ConstraintInfo *info)
@@ -1368,13 +1378,7 @@ constrain_size_increments (MetaWindow         *window,
     new_height = client_rect.height;
   }
 
-  /* Figure out what original rect to pass to meta_rectangle_resize_with_gravity
-   * See bug 448183
-   */
-  if (info->action_type == ACTION_MOVE_AND_RESIZE)
-    start_rect = &info->current;
-  else
-    start_rect = &info->orig;
+  start_rect = get_start_rect_for_resize (window, info);
 
   /* Resize to the new size */
   meta_rectangle_resize_with_gravity (start_rect,
@@ -1424,13 +1428,7 @@ constrain_size_limits (MetaWindow         *window,
   new_width  = CLAMP (info->current.width,  min_size.width,  max_size.width);
   new_height = CLAMP (info->current.height, min_size.height, max_size.height);
 
-  /* Figure out what original rect to pass to meta_rectangle_resize_with_gravity
-   * See bug 448183
-   */
-  if (info->action_type == ACTION_MOVE_AND_RESIZE)
-    start_rect = &info->current;
-  else
-    start_rect = &info->orig;
+  start_rect = get_start_rect_for_resize (window, info);
 
   meta_rectangle_resize_with_gravity (start_rect,
                                       &info->current,
@@ -1570,13 +1568,7 @@ constrain_aspect_ratio (MetaWindow         *window,
     new_height = client_rect.height;
   }
 
-  /* Figure out what original rect to pass to meta_rectangle_resize_with_gravity
-   * See bug 448183
-   */
-  if (info->action_type == ACTION_MOVE_AND_RESIZE)
-    start_rect = &info->current;
-  else
-    start_rect = &info->orig;
+  start_rect = get_start_rect_for_resize (window, info);
 
   meta_rectangle_resize_with_gravity (start_rect,
                                       &info->current,
