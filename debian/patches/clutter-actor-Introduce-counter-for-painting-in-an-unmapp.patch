From: =?utf-8?q?Jonas_Dre=C3=9Fler?= <verdre@v0yd.nl>
Date: Sat, 11 Jul 2020 09:59:47 +0000
Subject: clutter/actor: Introduce counter for painting in an unmapped branch

Just like the existing in_cloned_branch counter, add a property which
tracks whether the actor is part of a subtree that's being painted while
unmapped. This is going to be useful for a few things, for example
changing the clutter_actor_is_in_clone_paint() API to use
enable_paint_unmapped instead of in_clone_paint.

https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/1366

(cherry picked from commit bf7cfb877c9d88e9995b56e6e82fa103bad39015)

Origin: upstream, 3.38.2
---
 clutter/clutter/clutter-actor.c | 49 ++++++++++++++++++++++++++++++++++++++++-
 1 file changed, 48 insertions(+), 1 deletion(-)

diff --git a/clutter/clutter/clutter-actor.c b/clutter/clutter/clutter-actor.c
index 2198b96..47c9171 100644
--- a/clutter/clutter/clutter-actor.c
+++ b/clutter/clutter/clutter-actor.c
@@ -804,6 +804,8 @@ struct _ClutterActorPrivate
    */
   gulong in_cloned_branch;
 
+  guint unmapped_paint_branch_counter;
+
   GListModel *child_model;
   ClutterActorCreateChildFunc create_child_func;
   gpointer create_child_data;
@@ -1081,6 +1083,11 @@ static void clutter_actor_push_in_cloned_branch (ClutterActor *self,
 static void clutter_actor_pop_in_cloned_branch (ClutterActor *self,
                                                 gulong        count);
 
+static void push_in_paint_unmapped_branch (ClutterActor *self,
+                                           guint         count);
+static void pop_in_paint_unmapped_branch (ClutterActor *self,
+                                          guint         count);
+
 static GQuark quark_actor_layout_info = 0;
 static GQuark quark_actor_transform_info = 0;
 static GQuark quark_actor_animation_info = 0;
@@ -4354,6 +4361,9 @@ clutter_actor_remove_child_internal (ClutterActor                 *self,
   if (self->priv->in_cloned_branch)
     clutter_actor_pop_in_cloned_branch (child, self->priv->in_cloned_branch);
 
+  if (self->priv->unmapped_paint_branch_counter)
+    pop_in_paint_unmapped_branch (child, self->priv->unmapped_paint_branch_counter);
+
   /* if the child that got removed was visible and set to
    * expand then we want to reset the parent's state in
    * case the child was the only thing that was making it
@@ -11998,6 +12008,9 @@ clutter_actor_add_child_internal (ClutterActor              *self,
   if (self->priv->in_cloned_branch)
     clutter_actor_push_in_cloned_branch (child, self->priv->in_cloned_branch);
 
+  if (self->priv->unmapped_paint_branch_counter)
+    push_in_paint_unmapped_branch (child, self->priv->unmapped_paint_branch_counter);
+
   /* children may cause their parent to expand, if they are set
    * to expand; if a child is not expanded then it cannot change
    * its parent's state. any further change later on will queue
@@ -14595,10 +14608,15 @@ _clutter_actor_set_enable_paint_unmapped (ClutterActor *self,
 
   priv = self->priv;
 
+  if (priv->enable_paint_unmapped == enable)
+    return;
+
   priv->enable_paint_unmapped = enable;
 
-  if (priv->enable_paint_unmapped)
+  if (enable)
     {
+      push_in_paint_unmapped_branch (self, 1);
+
       /* Make sure that the parents of the widget are realized first;
        * otherwise checks in clutter_actor_update_map_state() will
        * fail.
@@ -14614,6 +14632,7 @@ _clutter_actor_set_enable_paint_unmapped (ClutterActor *self,
   else
     {
       clutter_actor_update_map_state (self, MAP_STATE_CHECK);
+      pop_in_paint_unmapped_branch (self, 1);
     }
 }
 
@@ -19600,6 +19619,34 @@ clutter_actor_has_mapped_clones (ClutterActor *self)
   return FALSE;
 }
 
+static void
+push_in_paint_unmapped_branch (ClutterActor *self,
+                               guint         count)
+{
+  ClutterActor *iter;
+
+  for (iter = self->priv->first_child;
+       iter != NULL;
+       iter = iter->priv->next_sibling)
+    push_in_paint_unmapped_branch (iter, count);
+
+  self->priv->unmapped_paint_branch_counter += count;
+}
+
+static void
+pop_in_paint_unmapped_branch (ClutterActor *self,
+                              guint         count)
+{
+  ClutterActor *iter;
+
+  self->priv->unmapped_paint_branch_counter -= count;
+
+  for (iter = self->priv->first_child;
+       iter != NULL;
+       iter = iter->priv->next_sibling)
+    pop_in_paint_unmapped_branch (iter, count);
+}
+
 static void
 clutter_actor_child_model__items_changed (GListModel *model,
                                           guint       position,
